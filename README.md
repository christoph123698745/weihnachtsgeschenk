Gebaut und getestet mit Python 3.7.9, könnte aber mit beliebigen Python-Versionen >= 3.6 laufen.

# Bedienung
Am meisten Spaß macht das Weihnachtsgeschenk mit [`ipython`](https://ipython.readthedocs.io/), es kann aber grundsätzlich mit jeder Python-Shell bedient werden.

Starten Sie eine Python-Shell im Hauptverzeichnis und importieren Sie sich zu Beginn ein Weihnachtsgeschenk vom Nordpol und ein Wohnzimmer aus dem Möbelhaus:

`from nordpol import Weihnachtsgeschenk`

`from moebelhaus import wohnzimmer`

Viel Spaß!

Kleine Tipps für den Einstieg:
- ein `Weihnachtsgeschenk` möchte zuerst erzeugt werden, z. B. so: `weihnachtsgeschenk = Weihnachtsgeschenk()`
- die Systemzeit wird genutzt, um zu erkennen, ob aktuell Weihnachten ist
