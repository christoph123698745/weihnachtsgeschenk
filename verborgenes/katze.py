import random


class SchroedingersKatze:
    def anstupsen(self):
        if not self._katze_lebt:
            return "Die Katze liegt leblos auf dem Boden."
        self._naechster_zustand()
        return "Die Katze schreckt auf und rennt durch das Zimmer."

    def _naechster_zustand(self):
        self.__class__ = RennendeKatze

    def __init__(self):
        self._katze_lebt = random.choice((True, False))

    def __repr__(self):
        return "eine Katze"


class RennendeKatze:
    def _naechster_zustand(self):
        self.__class__ = KatzeImKarton

    def __repr__(self):
        return "eine rennende Katze"


class KatzeImKarton:
    def empathisch_begutachten(self):
        return "Die Katze sieht sehr glücklich aus."

    def rational_begutachten(self):
        return "Die Katze ist im Karton."

    def streicheln(self):
        return "Schnurr."

    __repr__ = SchroedingersKatze.__repr__
