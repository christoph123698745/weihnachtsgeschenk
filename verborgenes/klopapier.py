import random


class Klopapier:
    _LAGEN = {
        1: 'hauchdünn',
        2: 'dünn',
        3: 'durchschnittlich',
        4: 'reißfest',
        5: 'reißfest und komfortabel'
    }

    def testen(self):
        return f"Das Klopapier hat {self._qualitaet} Lage{'n' if self._qualitaet > 1 else ''} und ist {self._LAGEN[self._qualitaet]}."

    def _naechster_zustand(self):
        self.__class__ = KlopapierImKarton

    def __init__(self):
        self._qualitaet = random.choice(list(self._LAGEN))

    def __repr__(self):
        return "eine Packung Klopapier"


class KlopapierImKarton:
    __repr__ = Klopapier.__repr__
