from . import baum, botschaften, ereignisse, jeff, katze, klopapier, knopf, streichholz, verpackung
from .weihnachten import ist_weihnachten
from .wohnzimmer import wohnzimmer

_DURCHGESPIELT = False
_GESCHENK_IST_DA = False


class Weihnachtsgeschenk:
    def am_geschenkband_zupfen(self):
        if not ist_weihnachten():
            print(
                "Autsch!\n"
                "Sie haben sich am scharfen Rand des Geschenkbands geschnitten und weichen erschrocken zurück.\n"
                "Sie erinnern sich daran, dass Geschenkbänder nur an Weihnachten weich und geschmeidig sind.\n"
            )
            #
            #
            raise botschaften.KeinWeihnachtenError()
            #
            #
        self._naechster_zustand()
        return wohnzimmer._hinzufuegen(verpackung.Geschenkband())

    def _naechster_zustand(self):
        self.__class__ = WeihnachtsgeschenkOhneBand
        self.__init__()

    def __init__(self):
        global _DURCHGESPIELT, _GESCHENK_IST_DA
        if not _DURCHGESPIELT and _GESCHENK_IST_DA:
            #
            #
            raise botschaften.ZuVieleGeschenkeError()
            #
            #
        _GESCHENK_IST_DA = True

    def __repr__(self):
        return "ein Weihnachtsgeschenk"


class WeihnachtsgeschenkOhneBand:
    def geschenkpapier_bewundern(self):
        print("Das Papier ist wirklich sehr schön.")

    def geschenkpapier_aufreissen(self):
        if self._klebestreifen > 0:
            #
            #
            raise botschaften.KlebestreifenError()
            #
            #
        self._naechster_zustand()
        return wohnzimmer._hinzufuegen(verpackung.Geschenkpapier())

    def klebestreifen_abreissen(self):
        if self._klebestreifen < 1:
            #
            #
            raise botschaften.KeineKlebestreifenError()
            #
            #
        self._klebestreifen -= 1
        return wohnzimmer._hinzufuegen(verpackung.Klebestreifen())

    def _naechster_zustand(self):
        self.__class__ = WeihnachtsgeschenkOhneGeschenkpapier

    def __init__(self):
        self._klebestreifen = 3

    __repr__ = Weihnachtsgeschenk.__repr__


class WeihnachtsgeschenkOhneGeschenkpapier:
    def oeffnen(self):
        self._naechster_zustand()
        return (
            wohnzimmer._hinzufuegen(katze.SchroedingersKatze()),
            wohnzimmer._hinzufuegen(streichholz.Streichholzschachtel()),
            wohnzimmer._hinzufuegen(knopf.Knopf()),
        )

    def _naechster_zustand(self):
        self.__class__ = Karton
        self.__init__()

    __repr__ = Weihnachtsgeschenk.__repr__


class Karton:
    def inspizieren(self):
        if not self._inhalt:
            return "Im Karton befindet sich nichts."
        if len(self._inhalt) == 1:
            return f"Im Karton befindet sich {self._inhalt[0]}."
        inhalt = (self._inhalt_ele_str(i, ele, len(self._inhalt)) for i, ele in enumerate(self._inhalt))
        return f"Im Karton befinden sich {''.join(inhalt)}."

    def _naechster_zustand(self):
        self.__class__ = ZugeklebterKarton

    @staticmethod
    def _inhalt_ele_str(i, ele, length):
        if i == length - 1:
            return f"{ele}"
        if i == length - 2:
            return f"{ele} und "
        return f"{ele}, "

    def __init__(self):
        self._inhalt = []
        self._fehlende_klebestreifen = 3

    def __repr__(self):
        if not self._inhalt:
            return "ein leerer Karton"
        return "ein Karton"

    def __add__(self, other):
        if isinstance(other, katze.RennendeKatze):
            return ereignisse.katze_in_karton_stecken(other, self)
        if isinstance(other, baum.AbgebrannterWeihnachtsbaum):
            return ereignisse.abgebrannten_weihnachtsbaum_in_karton_stecken(other, self)
        if isinstance(other, klopapier.Klopapier):
            return ereignisse.klopapier_in_karton_stecken(other, self)
        if isinstance(other, verpackung.Klebestreifen):
            return ereignisse.klebestreifen_auf_karton_kleben(other, self)
        return "Nichts passiert."

    __radd__ = __add__


class ZugeklebterKarton:
    def _naechster_zustand(self):
        self.__class__ = Retoure

    def __repr__(self):
        return "ein zugeklebter Karton"

    def __add__(self, other):
        if isinstance(other, jeff.Ruecksendeschein):
            return ereignisse.ruecksendeschein_auf_karton_kleben(other, self)
        return "Nichts passiert."

    __radd__ = __add__


class Retoure:
    def __repr__(self):
        return "eine Retoure"
