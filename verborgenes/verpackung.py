from .weihnachten import ist_weihnachten


class Geschenkband:
    def __repr__(self):
        if not ist_weihnachten():
            return "ein Geschenkband mit scharfem Rand"
        return "ein weiches und geschmeidiges Geschenkband"


class Klebestreifen:
    def funktion_pruefen(self):
        return "Der Klebestreifen klebt noch gut."

    def _naechster_zustand(self):
        self.__class__ = KlebestreifenAufKarton

    def __repr__(self):
        return "ein Klebestreifen"


class KlebestreifenAufKarton:
    def __repr__(self):
        return "ein aufgeklebter Klebestreifen"


class Geschenkpapier:
    def wegwerfen(self):
        print("Das Geschenkpapier verschwindet traurig im Mülleimer.")
        self._naechster_zustand()

    def _naechster_zustand(self):
        self.__class__ = WeggeworfenesGeschenkpapier

    def __repr__(self):
        return "ein sehr schönes Geschenkpapier"


class WeggeworfenesGeschenkpapier:
    def __repr__(self):
        return "ein weggeworfenes Geschenkpapier"
