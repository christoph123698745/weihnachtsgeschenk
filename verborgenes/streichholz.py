import random

from . import ereignisse
from .weihnachten import ist_weihnachten
from .wohnzimmer import wohnzimmer


class Streichholzschachtel:
    def streichholz_rausnehmen(self):
        if self._streichhoelzer < 1:
            return "Die Streichholzschachtel ist leer."
        if self._streichhoelzer == self._ablenkendes_streichholz and ist_weihnachten():
            self._streichhoelzer -= 1
            return wohnzimmer._hinzufuegen(AblenkendesStreichholz())
        self._streichhoelzer -= 1
        return wohnzimmer._hinzufuegen(Streichholz())

    def __init__(self):
        self._streichhoelzer = 17
        self._ablenkendes_streichholz = random.randint(1, 17)

    def __repr__(self):
        return "eine Streichholzschachtel"

    def __add__(self, other):
        if isinstance(other, Streichholz):
            return ereignisse.streichholz_anzuenden(other)
        if isinstance(other, AblenkendesStreichholz):
            return ereignisse.weihnachtsbaum_anzuenden(other)
        return "Nichts passiert."

    __radd__ = __add__


class Streichholz:
    def _naechster_zustand(self):
        self.__class__ = AbgebranntesStreichholz

    def __repr__(self):
        return "ein Streichholz"


class AblenkendesStreichholz:
    _naechster_zustand = Streichholz._naechster_zustand
    __repr__ = Streichholz.__repr__


class AbgebranntesStreichholz:
    def __repr__(self):
        return "ein abgebranntes Streichholz"
