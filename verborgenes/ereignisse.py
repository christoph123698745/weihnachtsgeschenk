from time import sleep

from . import baum, katze, klopapier
from .wohnzimmer import wohnzimmer


def katze_in_karton_stecken(rennende_katze, karton):
    if katze.KatzeImKarton in map(type, karton._inhalt):
        return "Es ist schon eine Katze im Karton."
    rennende_katze._naechster_zustand()
    karton._inhalt.append(rennende_katze)
    wohnzimmer._entfernen(rennende_katze)
    return "Die Katze hat aufgehört zu rennen und ist in den Karton gekrabbelt."


def streichholz_anzuenden(streichholz):
    print("Sie entzünden das Streichholz.")
    sleep(1)
    print("...")
    sleep(1)
    print("Das Streichholz brennt sehr gut.")
    sleep(1)
    print("...")
    sleep(1)
    print("Sie beobachten gespannt, wie sich die Flamme Ihrer Hand nähert.")
    sleep(1)
    print("...")
    sleep(1)
    streichholz._naechster_zustand()
    print("Sie pusten das Streichholz aus, bevor die Flamme Ihre Hand erreicht.")


def weihnachtsbaum_anzuenden(streichholz):
    print("Sie entzünden das Streichholz")
    sleep(1)
    print("...")
    sleep(1)
    print("Das Streichholz brennt sehr gut.")
    sleep(1)
    print("...")
    sleep(1)
    print("Sie müssen plötzlich an das Regelwerk der UEFA Nations League denken.")
    sleep(1)
    print("...")
    sleep(1)
    print("...")
    sleep(1)
    print("...")
    sleep(1)
    print("Die Flamme des brennenden Streichholzes erreicht Ihre Hand und Sie verbrennen sich.")
    sleep(1)
    print("...")
    sleep(1)
    print("Reflexartig lassen Sie das noch brennende Streichholz fallen.")
    sleep(1)
    print("...")
    sleep(1)
    print("Das brennende Streichholz fällt in den Weihnachtsbaum.")
    sleep(1)
    print("...")
    sleep(1)
    streichholz._naechster_zustand()
    return wohnzimmer._hinzufuegen(baum.BrennenderWeihnachtsbaum())


def abgebrannten_weihnachtsbaum_in_karton_stecken(abgebrannter_weihnachtsbaum, karton):
    if baum.AbgebrannterWeihnachtsbaumImKarton in map(type, karton._inhalt):
        return "Es ist schon ein abgebrannter Weihnachtsbaum im Karton."
    abgebrannter_weihnachtsbaum._naechster_zustand()
    karton._inhalt.append(abgebrannter_weihnachtsbaum)
    wohnzimmer._entfernen(abgebrannter_weihnachtsbaum)
    return "Der abgebrannte Weihnachtsbaum liegt jetzt im Karton."


def klopapier_in_karton_stecken(packung_klopapier, karton):
    if klopapier.KlopapierImKarton in map(type, karton._inhalt):
        return "Es ist schon eine Packung Klopapier im Karton."
    packung_klopapier._naechster_zustand()
    karton._inhalt.append(packung_klopapier)
    wohnzimmer._entfernen(packung_klopapier)
    return "Die Packung Klopapier liegt jetzt im Karton."


def klebestreifen_auf_karton_kleben(klebestreifen, karton):
    if not karton._inhalt:
        return "Einen Klebestreifen auf einen leeren Karton zu kleben, ergibt keinen Sinn."
    klebestreifen._naechster_zustand()
    karton._fehlende_klebestreifen -= 1
    wohnzimmer._entfernen(klebestreifen)
    if karton._fehlende_klebestreifen < 1:
        karton._naechster_zustand()
        return "Der Karton ist jetzt zugeklebt."
    return f"Der Klebestreifen klebt jetzt auf dem Karton. Es fehl{'t' if karton._fehlende_klebestreifen == 1 else 'en'} noch {karton._fehlende_klebestreifen} Klebestreifen, um den Karton zu verschließen."


def ruecksendeschein_auf_karton_kleben(ruecksendeschein, karton):
    ruecksendeschein._naechster_zustand()
    karton._naechster_zustand()
    wohnzimmer._entfernen(ruecksendeschein)
    return "Der Rücksendeschein klebt jetzt auf dem Karton."
