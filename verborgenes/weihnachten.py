import datetime


def ist_weihnachten():
    heute = datetime.date.today()
    weihnachten_anfang = datetime.date(year=heute.year, month=12, day=24)
    weihnachten_ende = datetime.date(year=heute.year, month=12, day=26)
    return weihnachten_anfang <= heute <= weihnachten_ende
