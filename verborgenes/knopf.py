from time import sleep

from . import botschaften, jeff, klopapier
from .wohnzimmer import wohnzimmer


class Knopf:
    def __init__(self):
        self._bestellungen = [jeff.JeffBezos(), klopapier.Klopapier()]

    def __repr__(self):
        return "ein Knopf"

    def druecken(self):
        if not self._bestellungen:
            #
            #
            raise botschaften.KnopfKlemmtError()
            #
            #
        print("Vielen Dank für Ihre Bestellung!")
        sleep(1)
        print("...")
        sleep(1)
        return wohnzimmer._hinzufuegen(self._bestellungen.pop())
