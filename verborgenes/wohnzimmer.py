class Wohnzimmer(list):
    def anleitung(self):
        print(
            "Im Wohnzimmer werden Objekte gesammelt, damit sie nicht verloren gehen.\n"
            "Das Wohnzimmer ist im Grunde eine Python-list und lässt sich auch so bedienen.\n"
            "Wird das Wohnzimmer aufgerufen, gibt es seinen Inhalt aus.\n"
            "Zusätzlich kann per wohnzimmer['<objektname>'] auf Objekte im Inventar zugegriffen werden.\n"
            "Beispiel: wohnzimmer['ein Weihnachtsgeschenk']\n"
        )

    def _hinzufuegen(self, objekt):
        if not objekt in self:
            self.append(objekt)
        return objekt

    def _entfernen(self, objekt):
        if objekt not in self:
            return
        self.remove(objekt)

    @staticmethod
    def _inhalt_ele_str(i, ele, length):
        if i == length - 1:
            return f"{ele}"
        if i == length - 2:
            return f"{ele} und "
        return f"{ele}, "

    def __repr__(self):
        if not self:
            return "Im Wohnzimmer befindet sich nichts."
        if len(self) == 1:
            return f"Im Wohnzimmer befindet sich {self[0]}."
        inhalt = (self._inhalt_ele_str(i, ele, len(self)) for i, ele in enumerate(self))
        return f"Im Wohnzimmer befinden sich {''.join(inhalt)}."

    def __getitem__(self, item):
        if not isinstance(item, str):
            #
            #
            return super().__getitem__(item)
            #
            #
        for objekt in self:
            if item.lower() == str(objekt).lower():
                return objekt
        #
        #
        raise KeyError("Das Objekt befindet sich nicht im Wohnzimmer.")
        #
        #


wohnzimmer = Wohnzimmer()
