from time import sleep

from . import botschaften, geschenk
from .wohnzimmer import wohnzimmer


class JeffBezos:
    def ansprechen(self):
        return "Hi, ich bin Jeff Bezos."

    def kontostand_abfragen(self):
        #
        #
        raise OverflowError("Der Kontostand ist zu groß.")
        #
        #

    def ruecksendeschein_anfordern(self):
        self._naechster_zustand()
        return wohnzimmer._hinzufuegen(Ruecksendeschein())

    def _naechster_zustand(self):
        self.__class__ = JeffBezosOhneRuecksendeschein

    def __repr__(self):
        return "ein Jeff Bezos"


class JeffBezosOhneRuecksendeschein:
    ansprechen = JeffBezos.ansprechen
    kontostand_abfragen = JeffBezos.kontostand_abfragen

    def retoure_zurueckschicken(self, retoure):
        if not isinstance(retoure, geschenk.Retoure):
            #
            #
            raise botschaften.KeineRetoureError()
            #
            #
        print("Vielen Dank für Ihre Rücksendung.")
        sleep(1)
        print("...")
        sleep(1)
        print(f"Sie haben {len(retoure._inhalt)} von 3 Artikeln zurückgesendet.")
        sleep(1)
        print("...")
        sleep(1)
        print("...")
        sleep(1)
        print("...")
        sleep(1)
        print("Hier ist Ihre Ersatzbestellung!")
        sleep(1)
        print("...")
        sleep(1)
        geschenk._DURCHGESPIELT = True
        self._naechster_zustand()
        return wohnzimmer._hinzufuegen(geschenk.Weihnachtsgeschenk())

    def _naechster_zustand(self):
        self.__class__ = JeffBezosOhneJob

    __repr__ = JeffBezos.__repr__


class JeffBezosOhneJob:
    def ansprechen(self):
        return (
            "Ich habe leider meinen Job bei Amazon verloren, da in der letzten Rücksendung Artikel waren, " 
            "die nicht der Amazon-Rücksende-Policy entsprachen."
        )

    __repr__ = JeffBezos.__repr__


class Ruecksendeschein:
    def _naechster_zustand(self):
        self.__class__ = AufgeklebterRuecksendeschein

    def __repr__(self):
        return "ein Rücksendeschein"


class AufgeklebterRuecksendeschein:
    def __repr__(self):
        return "ein aufgeklebter Rücksendeschein"
