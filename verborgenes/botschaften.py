class KeinWeihnachtenError(RuntimeError):
    def __init__(self):
        super().__init__("Es ist gerade kein Weihnachten.")


class ZuVieleGeschenkeError(RuntimeError):
    def __init__(self):
        super().__init__("Sie haben schon ein Geschenk.")


class KlebestreifenError(RuntimeError):
    def __init__(self):
        super().__init__("Es gibt noch Klebestreifen, die das Aufreißen verhindern.")


class KeineKlebestreifenError(RuntimeError):
    def __init__(self):
        super().__init__("Es gibt keine Klebestreifen mehr.")


class KnopfKlemmtError(RuntimeError):
    def __init__(self):
        super().__init__("Der Knopf klemmt und lässt sich nicht mehr drücken.")


class KeineRetoureError(RuntimeError):
    def __init__(self):
        super().__init__("Ohne Rücksendeschein kann ich leider nichts zurückschicken.")
