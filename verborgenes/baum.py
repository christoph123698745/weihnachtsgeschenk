class BrennenderWeihnachtsbaum:
    def die_lodernden_flammen_betrachten(self):
        return "Starren Sie nicht so und löschen Sie das Feuer!"

    def _naechster_zustand(self):
        self.__class__ = AbgebrannterWeihnachtsbaum

    def __init__(self):
        self.feuer = 'das Feuer'

    def __repr__(self):
        return "ein brennender Weihnachtsbaum"

    def __delattr__(self, item):
        if item == 'feuer':
            self._naechster_zustand()
            print("Sie haben den brennenden Baum gelöscht.")
        del(self.__dict__[item])


class AbgebrannterWeihnachtsbaum:
    def betrachten(self):
        return "Er hat definitiv ein paar Nadeln verloren."

    def beschnuppern(self):
        return "Riecht nach Waldbrand."

    def _naechster_zustand(self):
        self.__class__ = AbgebrannterWeihnachtsbaumImKarton

    def __repr__(self):
        return "ein abgebrannter Weihnachtsbaum"


class AbgebrannterWeihnachtsbaumImKarton:
    betrachten = AbgebrannterWeihnachtsbaum.betrachten
    beschnuppern = AbgebrannterWeihnachtsbaum.beschnuppern
    __repr__ = AbgebrannterWeihnachtsbaum.__repr__
